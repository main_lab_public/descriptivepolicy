# System-Agnostic Meta-Learning for MDP-based Dynamic Scheduling via Descriptive Policy
Python implementation of the system-agnostic meta-learning for MDP-based dynamic scheduling via descriptive policy.

## Usage
```
run_experiment.py --scenario SCENARIO
```
Required argument:
*  --scenario: types of scenario {A: simple explanatory, B: realistic application in wireless networks}
             
Example 
```train
python run_experiment.py --scenario A
```

Parameters of each scenario can be adjusted in:
*  Scenario A: scenarioA.scenario_A.py
*  Scenario B: scenarioB.parameters.py
                        
## Results
*  This implementation produces the data used in Figure 2 and Figure 3 in the paper.
*  Plotting data is not implemented here. We plotted the figures by using other applications.
*  All the results including the results in the supplementary material can be produced by very slightly modifying this implementation.
