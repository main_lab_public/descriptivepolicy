import numpy as np
from utils.utils import *


class Env:
    def __init__(self, env_param, network_param, system_settings):
        self.system_settings = system_settings
        self.sub_iteration = int(env_param['sub_iteration'])
        self.total_iteration = int(self.sub_iteration * len(system_settings))
        self.pathloss_coeff = env_param['pathloss_coeff']
        self.log_normal = env_param['log_normal']
        self.bandwidth = env_param['bandwidth']
        self.noise = db2normal(env_param['noise_density']) * 1e6
        self.max_trans_pwr = env_param['max_trans_pwr']
        self.discounted_factor = env_param['discounted_factor']
        self.initial_mu = env_param['initial_mu']
        self.max_mu = env_param['max_mu']
        self.min_h = env_param['min_h']
        self.max_h = env_param['max_h']

        self.user_no = None
        self.user_distance = None
        self.pathloss = None
        self.pathloss_dB = None
        self.rate_req = None

        # channel partition
        self.h_partition = np.linspace(self.min_h, self.max_h, network_param['h_partition_no'] - 1)
        self.h_no = np.alen(self.h_partition) + 1

        # Lagrangian multiplier partition
        if network_param['mu_partition_mode'] == 'square':
            self.mu_partition = np.linspace(0, np.sqrt(self.max_mu),
                                            network_param['mu_partition_no'])  # log scale
            self.mu_partition = self.mu_partition ** 2
        self.mu_no = np.alen(self.mu_partition)

        # power control partition
        if network_param['p_partition_mode'] == 'lin':
            self.p_partition = np.linspace(0, self.max_trans_pwr, network_param['p_partition_no'])
        self.p_no = np.alen(self.p_partition)

        self.feature_no = network_param['feature_no']

        self.channel = None
        self.shadowing = None
        self.next_shadowing = None
        self.iteration = 0
        self.accum_iter = 0

    def system_ch(self, system):
        if system == '1':
            user_no = 4
            user_distance = [20, 50, 50, 80]
            rate_req = list(np.ones(user_no) * 1)

        elif system == '2':
            user_no = 9
            user_distance = [20, 20, 20, 50, 50, 50, 80, 80, 80]
            rate_req = list(np.ones(user_no) * 0.5)

        elif system == '3':
            user_no = 20
            user_distance = np.zeros(user_no)
            user_distance[0:5] = 20
            user_distance[5:15] = 50
            user_distance[15:20] = 80
            rate_req = list(np.ones(user_no) * 0.2)

        return user_no, user_distance, rate_req

    def setting(self, system_no):
        user_no, user_distance, rate_req = self.system_ch(self.system_settings[system_no])
        self.user_no = user_no
        self.user_distance = user_distance
        self.pathloss = np.asarray(self.user_distance) ** -self.pathloss_coeff
        self.pathloss_dB = normal2db(self.pathloss)
        self.rate_req = np.asarray(rate_req)

    def channel_generator(self):
        shadowing = np.random.randn(self.user_no) * self.log_normal

        return shadowing

    def update(self):
        self.shadowing = self.next_shadowing
        self.channel = self.pathloss * db2normal(self.shadowing)
        self.next_shadowing = self.channel_generator()
