import numpy as np
from utils.utils import *

import torch
import torch.optim as optim
import torch.nn.functional as F


class DQNAgent:
    def __init__(self, agent_name, agent_mode, parameters, system_settings, env):
        self.agent_name = agent_name
        self.agent_mode = agent_mode
        self.parameters = parameters
        self.system_settings = system_settings

        self.lr = parameters.network['lr']
        self.epsilon = parameters.network['epsilon']
        self.batch_size = parameters.network['batch_size']
        self.replay_limit = parameters.network['replay_limit']
        self.h_no = parameters.network['h_partition_no']
        self.mu_no = parameters.network['mu_partition_no']
        self.p_no = parameters.network['p_partition_no']
        self.feature_no = parameters.network['feature_no']
        self.fc_variable_no = parameters.network['fc_variable_no']


        self.env = env

        self.system = None

        self.global_name = self.agent_name + '_global'
        self.target_name = self.agent_name + '_target'

        if self.agent_mode is 'Desc':
            self.desc_x_dim = self.h_no * self.mu_no
            self.desc_y_dim = self.p_no * self.h_no * self.mu_no
            self.main_network = DQN(self.global_name, self.fc_variable_no, self.desc_x_dim, self.desc_y_dim)
            self.target_network = DQN(self.target_name, self.fc_variable_no, self.desc_x_dim, self.desc_y_dim)
            self.target_network.load_state_dict(self.main_network.state_dict())
            self.target_network.eval()
            self.optimizer = optim.Adam(self.main_network.parameters(), lr=self.lr)
        else:
            self.main_network = None
            self.target_network = None

        self.experience_buffer = ReplayMemory(self.parameters.network['replay_limit'])

    def new_scenario(self, system):
        if self.agent_mode is 'Conv':
            del self.main_network
            del self.target_network
            conv_x_dim = self.env.user_no * self.feature_no
            conv_y_dim = self.env.user_no * self.p_no
            self.main_network = DQN(self.global_name, self.fc_variable_no, conv_x_dim, conv_y_dim)
            self.target_network = DQN(self.target_name, self.fc_variable_no, conv_x_dim, conv_y_dim)
            self.target_network.load_state_dict(self.main_network.state_dict())
            self.target_network.eval()
            self.optimizer = optim.Adam(self.main_network.parameters(), lr=self.lr)

        self.experience_buffer = ReplayMemory(self.parameters.network['replay_limit'])

        self.system = system

    def optimize_model(self):
        if len(self.experience_buffer) < self.parameters.network['batch_size']:
            return
        transitions = self.experience_buffer.sample(self.parameters.network['batch_size'])
        batch = Transition(*zip(*transitions))

        state_batch = torch.cat(batch.state)
        action_batch = torch.cat(batch.action)
        cost_batch = torch.cat(batch.utility)
        next_state_batch = torch.cat(batch.next_state)
        state_batch_stack = torch.stack(batch.state)

        state_action_values = self.main_network(state_batch).gather(1,action_batch)

        if self.agent_mode is 'Desc':
            next_state_values = self.target_network(next_state_batch).detach()
            for i in range(self.parameters.network['batch_size']):
                next_state_values[i,:] = next_state_values[i,:] + np.tile((state_batch_stack[i, :,:] == 0) * 1e10,
                                                                          (self.p_no, 1, 1)).flatten()
            next_state_values = next_state_values.min(1)[0]
        else:
            next_state_values = self.target_network(next_state_batch).min(1)[0].detach()
        expected_state_action_values = (next_state_values.reshape((self.parameters.network['batch_size'],1))
                                        * self.parameters.env['discounted_factor']) + cost_batch

        loss = F.mse_loss(state_action_values, expected_state_action_values)

        self.optimizer.zero_grad()
        loss.backward()
        for param in self.main_network.parameters():
            param.grad.data.clamp_(-100, 100)
        self.optimizer.step()

    def work(self, training=True):
        curr_state, curr_corr_user = self.system.get_state(self.agent_mode)

        action = self.scheduling(curr_state)
        scheduling, trans_pwr = self.interpret_action(self.env, action, curr_corr_user)
        cost = self.system.do_action(scheduling, trans_pwr)
        next_state, _ = self.system.get_state(self.agent_mode, next_state=True)

        if training:
            self.experience_buffer.push(torch.from_numpy(curr_state).float(),
                                        torch.tensor(action).reshape((-1,1)),
                                        torch.tensor(cost.reshape((-1,1))).float(),
                                        torch.from_numpy(next_state).float())

            if (self.env.iteration + 1) % self.parameters.network['train_interval'] == 0:
                self.optimize_model()

            if self.env.iteration % self.parameters.network['target_update_interval'] == 0:
                self.target_network.load_state_dict(self.main_network.state_dict())

    def scheduling(self, state):
        if self.agent_mode is 'Desc':
            rnd = np.random.rand(1)
            if rnd < self.epsilon:
                action_space = np.transpose(np.nonzero(state > 0))
                action_idx = random.sample(action_space.tolist(), 1)[0]
                p_idx = np.random.randint(self.p_no)

                action = np.ravel_multi_index([p_idx] + action_idx,
                                              (self.p_no, self.h_no, self.mu_no))
            else:
                state = torch.tensor(state).float()
                with torch.no_grad():
                    Q_value = self.main_network(state)
                    action = self.get_action(Q_value, state)
        elif self.agent_mode is 'Conv':
            rnd = np.random.rand(1)
            if rnd < self.epsilon:
                user_idx = np.random.randint(self.env.user_no)
                p_idx = np.random.randint(self.p_no)
                action = np.ravel_multi_index([user_idx] + [p_idx],
                                              (self.env.user_no, self.env.p_no))
            else:
                state = torch.tensor(state).float()
                with torch.no_grad():
                    Q_value = self.main_network(state)
                    action = self.get_action(Q_value, state)

        return action

    def get_action(self, Q_value, state):
        if self.agent_mode is 'Desc':
            Q_value = torch.reshape(Q_value, [-1, self.p_no, self.h_no, self.mu_no])
            Q_value = Q_value + np.tile((state == 0) * 1e10, (self.p_no, 1, 1))
            action = np.argmin(Q_value)
        elif self.agent_mode is 'Conv':
            action = np.argmin(Q_value)

        return action

    def interpret_action(self, env, action, corr_user=None):
        if self.agent_mode is 'Desc':
            action = list(np.unravel_index(action, (self.p_no, self.h_no, self.mu_no)))
            trans_pwr = env.p_partition[action[0]]
            scheduling = np.zeros(env.user_no)
            scheduling[random.sample(corr_user[action[1]][action[2]], 1)] = 1
        elif self.agent_mode is 'Conv':
            action = list(np.unravel_index(action, (self.env.user_no, self.p_no)))
            scheduling = np.zeros(env.user_no)
            scheduling[action[0]] = 1
            trans_pwr = env.p_partition[action[1]]

        return scheduling, trans_pwr