env = {
    'sub_iteration': 1e6,
    'pathloss_coeff': 3.76,
    'log_normal': 10,  # (dB)
    'bandwidth': 5,  # (Mhz)
    'max_trans_pwr': 10e3,  # (mW)
    'noise_density': -106,  # (dBm)
    'discounted_factor': 0.9,
    'initial_mu': 1.,
    'max_mu': 2,
    'min_h': -50,
    'max_h': -30
}

network = {
    'mu_partition_mode': 'square',  # log: log-scale, lin: linear-scale
    'mu_partition_no': 10,

    'h_partition_no': 5,

    'p_partition_mode': 'lin',  # dBm: dB-scale, lin: linear-scale
    'p_partition_no': 5,

    'feature_no': 2,

    'lr': 1e-4,
    'epsilon': 0.1,
    'batch_size': 30,
    'replay_limit': 300,
    'train_interval': 10,
    'target_update_interval': 100,
    'fc_variable_no': 300
}
