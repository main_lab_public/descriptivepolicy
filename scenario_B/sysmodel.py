import numpy as np


class SysModel:
    def __init__(self, env, parameters, train=True):
        if env.sub_iteration > 1e5:
            self.tooLarge = True
            self.avg_rate = np.zeros((env.user_no, int(1e5)))
            self.curr_avg_rate = None
            self.avg_pwr = np.zeros(int(1e5))
            self.accum_avg_pwr = np.zeros(int(1e5))
            self.curr_avg_pwr = None
            self.curr_accum_avg_pwr = None
            self.mu = np.zeros((env.user_no, int(1e5)))
        else:
            self.tooLarge = False
            self.avg_rate = np.zeros((env.user_no, env.sub_iteration))
            self.curr_avg_rate = None
            self.inst_rate = np.zeros((env.user_no, env.sub_iteration))
            self.avg_pwr = np.zeros(env.sub_iteration)
            self.accum_avg_pwr = np.zeros(env.sub_iteration)
            self.curr_avg_pwr = None
            self.curr_accum_avg_pwr = None
            self.pwr = np.zeros(env.sub_iteration)
            self.mu = np.zeros((env.user_no, env.sub_iteration))
        self.scheduled_no = np.zeros(env.user_no)
        self.parameters = parameters
        self.env = env
        self.rate_req = env.rate_req
        self.stepsize = 0.1
        self.train = train
        self.curr_mu = np.ones(env.user_no) * env.initial_mu

    def update(self, scheduling, trans_pwr, training=True):
        tmp_rate = scheduling * self.env.bandwidth \
                   * np.log2(1 + self.env.channel * trans_pwr / self.env.noise / self.env.bandwidth)
        if training is True:
            if self.env.iteration == 0:
                tmp_mu = np.maximum((self.curr_mu + self.stepsize * (self.rate_req - tmp_rate)),
                                    np.zeros(self.env.user_no))
            else:
                if self.train:
                    self.stepsize = max(min(0.1, 1 / (self.env.iteration + 1)), 1e-3)
                else:
                    self.stepsize = max(min(0.1, 1 / (self.env.iteration + 1)), 1e-3)
                tmp_mu = np.maximum((self.curr_mu + self.stepsize * (self.rate_req - tmp_rate)),
                                    np.zeros(self.env.user_no))
                tmp_mu = np.minimum(tmp_mu, np.ones(self.env.user_no) * self.env.max_mu)
        else:
            tmp_mu = self.curr_mu
        cost = (trans_pwr / 1e3 - np.sum(tmp_rate * self.curr_mu)) / 100

        self.scheduled_no = self.scheduled_no + (scheduling > 0)

        if self.tooLarge is True:
            if self.env.accum_iter == 0:
                self.curr_accum_avg_pwr = trans_pwr
            else:
                self.curr_accum_avg_pwr = (self.curr_accum_avg_pwr * self.env.accum_iter + trans_pwr) / (
                            self.env.accum_iter + 1)
            if self.env.iteration == 0:
                self.curr_avg_rate = tmp_rate
                self.curr_avg_pwr = trans_pwr
            else:
                self.curr_avg_rate = (self.curr_avg_rate * self.env.iteration + tmp_rate) / (self.env.iteration + 1)
                self.curr_avg_pwr = (self.curr_avg_pwr * self.env.iteration + trans_pwr) / (self.env.iteration + 1)
            self.curr_mu = tmp_mu

            if self.env.iteration % (self.env.sub_iteration / 1e5) == 0:
                self.accum_avg_pwr[int(self.env.iteration / (self.env.sub_iteration / 1e5))] = self.curr_accum_avg_pwr
                self.avg_pwr[int(self.env.iteration / (self.env.sub_iteration / 1e5))] = self.curr_avg_pwr
                self.avg_rate[:, int(self.env.iteration / (self.env.sub_iteration / 1e5))] = self.curr_avg_rate
                self.mu[:, int(self.env.iteration / (self.env.sub_iteration / 1e5))] = tmp_mu
        else:
            self.pwr[self.env.iteration] = trans_pwr
            if self.env.accum_iter == 0:
                self.curr_accum_avg_pwr = trans_pwr
                self.accum_avg_pwr[self.env.iteration] = trans_pwr
            else:
                self.curr_accum_avg_pwr = (self.curr_accum_avg_pwr * self.env.accum_iter + trans_pwr) / (
                            self.env.accum_iter + 1)
                self.accum_avg_pwr[self.env.iteration] = self.curr_accum_avg_pwr
            # average rate update
            self.inst_rate[:, self.env.iteration] = tmp_rate
            if self.env.iteration == 0:
                self.curr_avg_rate = tmp_rate
                self.avg_rate[:, self.env.iteration] = tmp_rate
                self.curr_avg_pwr = trans_pwr
                self.avg_pwr[self.env.iteration] = trans_pwr
            else:
                self.curr_avg_rate = (self.curr_avg_rate * self.env.iteration + tmp_rate) / (self.env.iteration + 1)
                self.avg_rate[:, self.env.iteration] = self.curr_avg_rate
                self.curr_avg_pwr = (self.curr_avg_pwr * self.env.iteration + trans_pwr) / (self.env.iteration + 1)
                self.avg_pwr[self.env.iteration] = self.curr_avg_pwr
            # mu update
            self.curr_mu = tmp_mu
            self.mu[:, self.env.iteration] = tmp_mu

        return cost, tmp_mu

    def get_state(self, mode, next_state=False):
        if mode is 'Desc':
            state = np.zeros((self.env.h_no, self.env.mu_no))
            corr_user = [[[] for _ in range(self.env.mu_no)] for _ in range(self.env.h_no)]

            for user_idx in range(self.env.user_no):
                if next_state is True:
                    h_idx, mu_idx = self.get_state_idx(
                        self.env.next_shadowing[user_idx] + self.env.pathloss_dB[user_idx],
                        self.curr_mu[user_idx])
                else:
                    h_idx, mu_idx = self.get_state_idx(self.env.shadowing[user_idx] + self.env.pathloss_dB[user_idx],
                                                       self.curr_mu[user_idx])

                state[h_idx, mu_idx] = 1
                corr_user[h_idx][mu_idx].append(user_idx)

            return state, corr_user
        elif mode is 'Conv':
            state = np.zeros((self.env.user_no, self.env.feature_no))

            for user_idx in range(self.env.user_no):
                if next_state is True:
                    state[user_idx, 0] = np.clip(
                        (self.env.next_shadowing[user_idx] + self.env.pathloss_dB[user_idx] - self.env.min_h)
                        / (self.env.max_h - self.env.min_h), 0, 1)
                else:
                    state[user_idx, 0] = np.clip(
                        (self.env.shadowing[user_idx] + self.env.pathloss_dB[user_idx] - self.env.min_h)
                        / (self.env.max_h - self.env.min_h), 0, 1)
                state[user_idx, 1] = np.sqrt(self.curr_mu[user_idx]) / np.sqrt(self.env.max_mu)

            return state, None

    def get_state_idx(self, shadowing, mu):
        h_idx = np.nonzero(self.env.h_partition <= shadowing)
        if h_idx[0].size:
            h_idx = np.max(h_idx) + 1
        else:
            h_idx = 0

        mu_idx = np.max(np.nonzero(self.env.mu_partition <= mu))

        return h_idx, mu_idx

    def do_action(self, scheduling, trans_pwr, training=True):
        cost, _ = self.update(scheduling, trans_pwr, training=training)

        return cost

    def get_dict(self):
        if self.tooLarge is True:
            mat_dict = {'avg_rate': self.avg_rate,
                        'avg_pwr': self.avg_pwr,
                        'accum_avg_pwr': self.accum_avg_pwr,
                        'scheduled_no': self.scheduled_no,
                        'mu': self.mu}
        else:
            mat_dict = {'avg_rate': self.avg_rate,
                        'inst_rate': self.inst_rate,
                        'avg_pwr': self.avg_pwr,
                        'accum_avg_pwr': self.accum_avg_pwr,
                        'pwr': self.pwr,
                        'scheduled_no': self.scheduled_no,
                        'mu': self.mu}

        return mat_dict
