import numpy as np
from tqdm import tqdm
from scenario_B.env import Env
from scenario_B.sysmodel import SysModel
from scenario_B.agent_dqn import DQNAgent
from scenario_B import parameters


def run():
    # Set scenarios
    system_settings = ['1', '2', '3']
    sys_no = len(system_settings)

    env = Env(parameters.env, parameters.network, system_settings)

    # agent names
    agent_names = ['DescDQN', 'ConvDQN']

    env.setting(0)
    # Create agents
    agents = []
    agents.append(DQNAgent(agent_names[0], 'Desc', parameters, system_settings, env))
    agents.append(DQNAgent(agent_names[1], 'Conv', parameters, system_settings, env))

    for system_no in range(sys_no):
        env.setting(system_no)
        if system_no != 0:
            tmp_curr_avg_pwr = []
            tmp_curr_avg_pwr.append(systems[0].curr_avg_pwr)
            tmp_curr_avg_pwr.append(systems[1].curr_avg_pwr)
            tmp_curr_accum_avg_pwr = []
            tmp_curr_accum_avg_pwr.append(systems[0].curr_accum_avg_pwr)
            tmp_curr_accum_avg_pwr.append(systems[1].curr_accum_avg_pwr)

        # Create systems
        systems = []
        systems.append(SysModel(env, parameters))
        systems.append(SysModel(env, parameters))
        if system_no != 0:
            systems[0].curr_avg_pwr = tmp_curr_avg_pwr[0]
            systems[1].curr_avg_pwr = tmp_curr_avg_pwr[1]
            systems[0].curr_accum_avg_pwr = tmp_curr_accum_avg_pwr[0]
            systems[1].curr_accum_avg_pwr = tmp_curr_accum_avg_pwr[1]

        agents[0].new_scenario(systems[0])
        agents[1].new_scenario(systems[1])

        env.next_shadowing = env.channel_generator()
        pbar = tqdm(range(env.sub_iteration),
                    desc=f'System {system_settings[system_no]}',
                    bar_format='{desc:<9}{percentage:3.0f}%|{bar:10}{r_bar}')
        for env.iteration in pbar:
            pbar.set_postfix({'Desc-P': systems[0].curr_avg_pwr, 'Conv-P': systems[1].curr_avg_pwr})
            env.update()
            if system_no == sys_no:
                agents[0].work(training=False)
            else:
                agents[0].work()
            agents[1].work()

            env.accum_iter = env.accum_iter + 1
