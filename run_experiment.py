import warnings

warnings.filterwarnings('ignore')

import argparse
from scenario_A import scenario_A
from scenario_B import scenario_B


def get_parser():
    parser = argparse.ArgumentParser(description="Experiments for 'System-Agnostic Meta-Learning for MDP-based"
                                                 "Dynamic Scheduling via Descriptive Policy'")

    parser.add_argument('--scenario', required=True, help='types of scenario {A: simple explanatory,'
                                                          'B: realistic application in wireless networks}')

    return parser


def main(args=None):
    parser = get_parser()
    args = parser.parse_args(args)

    scenario_list = ['A', 'B']

    if args.scenario not in scenario_list:
        raise ValueError('Invalid scenario.')

    if args.scenario == 'A':
        scenario_A.run()

    if args.scenario == 'B':
        scenario_B.run()


if __name__ == '__main__':
    main()
