import numpy as np
import torch.nn.functional as F
from utils.utils import *


class DescDQN(DQN):
    def __init__(self, name, fc_variable_no, x_no, p_no):
        super(DescDQN, self).__init__(name, fc_variable_no, x_no*p_no, x_no*p_no)


def to_descriptive(state, user_no, p_partition, x_max, x_no, p_no):
    descState = np.zeros((x_no, p_no))
    corr_user = [[[] for _ in range(p_no)] for _ in range(x_no)]

    for user_idx in range(user_no):
        if x_no == x_max + 1:
            x_idx = int(state[user_idx, 0])
        else:
            x_idx = int(np.floor(state[user_idx, 0]/(x_max+1)*x_no))
        p_idx = np.max(np.nonzero(p_partition <= state[user_idx, 1]))

        descState[x_idx, p_idx] = 1
        corr_user[x_idx][p_idx].append(user_idx)
    return descState, corr_user


def get_action_desc_dqn(t, epsilon, desc_curr_state, curr_corr_user, x_no, p_no, main_network):
    rnd = np.random.rand(1)
    if t > 1e4:
        eepsilon = 1 / t
    else:
        eepsilon = epsilon
    if rnd < eepsilon:
        action_space = np.transpose(np.nonzero(desc_curr_state > 0))
        action = random.sample(action_space.tolist(), 1)[0]
        action_idx = np.ravel_multi_index(action, (x_no, p_no))
    else:
        desc_curr_state_tensor = torch.tensor(desc_curr_state).float()
        Q_value = main_network(desc_curr_state_tensor).detach()
        Q_value = torch.reshape(Q_value, [x_no, p_no]).numpy()
        Q_value = Q_value - (desc_curr_state == 0) * 1e10
        action_idx = np.argmax(Q_value)
        action = list(np.unravel_index(action_idx, (x_no, p_no)))
    scheduling = random.sample(curr_corr_user[action[0]][action[1]], 1)

    return action_idx, scheduling


def desc_dqn_train(transitions, batch_size, main_network, target_network, optimizer, gamma):
    batch = Transition(*zip(*transitions))

    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.utility)
    next_state_batch = torch.cat(batch.next_state)
    state_batch_stack = torch.stack(batch.state)

    state_action_values = main_network(state_batch).gather(1, action_batch)
    next_state_values = target_network(next_state_batch).detach()
    for i in range(batch_size):
        next_state_values[i, :] = next_state_values[i, :] - (state_batch_stack[i, :] == 0).flatten() * 1e10
    next_state_values = next_state_values.max(1)[0]

    # 기대 Q 값 계산
    expected_state_action_values = (next_state_values.reshape((batch_size, 1)) * gamma) + reward_batch

    # Huber 손실 계산
    loss = F.mse_loss(state_action_values, expected_state_action_values)

    # 모델 최적화
    optimizer.zero_grad()
    loss.backward()
    for param in main_network.parameters():
        param.grad.data.clamp_(-100, 100)
        # print(sum(param.grad.data))
    optimizer.step()