import numpy as np
from scipy.stats import truncexpon
import torch.optim as optim
from tqdm import tqdm
from scenario_A.conv_policy import *
from scenario_A.desc_policy import *
from utils.utils import *


class system_ch:
    def __init__(self, N, x_max, p_min, p_max, p_no):
        self.N = N
        self.x_max = x_max
        self.p_min = p_min
        self.p_max = p_max

        self.x_no = x_max + 1
        self.p_no = p_no
        self.p_partition = np.linspace(p_min, p_max, self.p_no + 1)


def gen_state(user_no, x_max, p_min, p_max, p_gen_mode=0):
    state = np.zeros((user_no, 2))
    state[:, 0] = np.random.randint(0, x_max + 1, (user_no,))
    if p_gen_mode == 0:
        state[:, 1] = np.random.uniform(p_min, p_max, user_no)
    elif p_gen_mode == 1:
        state[:, 1] = (1 - truncexpon.rvs(5, size=user_no) / 5)

    return state


def run():
    systems = []
    systems.append(system_ch(2, 4, 0, 1, 4))
    systems.append(system_ch(6, 4, 0, 1, 4))
    systems.append(system_ch(10, 4, 0, 1, 4))

    sys_no = len(systems)

    total_iteration = 100000
    gamma = 0.9
    epsilon = 0.1
    fc_variable_no = 100
    train_interval = 10
    target_update_interval = 100
    batch_size = 30
    replay_limit = 300

    lr = 1e-3

    ### Conv-P
    ConvDQN_main = []
    ConvDQN_target = []
    ConvDQN_optimizer = []
    ConvDQN_buffer = []
    for i in range(len(systems)):
        ConvDQN_main.append(ConvDQN(f'main_es_network_{i}', fc_variable_no, systems[i].N))
        ConvDQN_target.append(ConvDQN(f'main_es_network_{i}', fc_variable_no, systems[i].N))
        ConvDQN_target[i].load_state_dict(ConvDQN_main[i].state_dict())
        ConvDQN_target[i].eval()
        ConvDQN_optimizer.append(optim.Adam(ConvDQN_main[i].parameters(), lr=lr))

        ConvDQN_buffer.append(ReplayMemory(replay_limit))

    DescDQN_main = DescDQN('main_network', fc_variable_no, systems[0].x_no, systems[0].p_no)
    DescDQN_target = DescDQN('target_network', fc_variable_no, systems[0].x_no, systems[0].p_no)
    DescDQN_target.load_state_dict(DescDQN_main.state_dict())
    DescDQN_target.eval()
    DescDQN_optimizer = optim.Adam(DescDQN_main.parameters(), lr=lr)

    DescDQN_buffer = ReplayMemory(replay_limit)

    dict_avg_reward_opt = np.zeros((total_iteration * sys_no,))
    dict_avg_reward_conv = np.zeros((total_iteration * sys_no,))
    dict_avg_reward_desc = np.zeros((total_iteration * sys_no,))

    Q_value = np.zeros((2, systems[0].x_no, systems[0].p_no))

    tt = 0

    ## systems
    for i in range(sys_no):
        if i <= 1:
            desc_state_tensor = torch.tensor(np.ones((systems[0].x_no, systems[0].p_no))).float()
            Q_value_tmp = DescDQN_main(desc_state_tensor).detach()
            Q_value[i, :, :] = torch.reshape(Q_value_tmp, [systems[0].x_no, systems[0].p_no]).numpy()

        avg_reward_opt = 0
        avg_reward_conv = 0
        avg_reward_desc = 0

        curr_state = gen_state(systems[i].N, systems[i].x_max, systems[i].p_min, systems[i].p_max)
        desc_curr_state, curr_corr_user = to_descriptive(curr_state, systems[i].N, systems[i].p_partition,
                                                         systems[i].x_max, systems[i].x_no, systems[i].p_no)
        next_state = gen_state(systems[i].N, systems[i].x_max, systems[i].p_min, systems[i].p_max)
        desc_next_state, next_corr_user = to_descriptive(next_state, systems[i].N, systems[i].p_partition,
                                                         systems[i].x_max, systems[i].x_no, systems[i].p_no)

        pbar = tqdm(range(int(total_iteration)),
                    desc=f'System {i + 1}',
                    bar_format='{desc:<9}{percentage:3.0f}%|{bar:10}{r_bar}')
        for t in pbar:
            pbar.set_postfix({'Opt-P': avg_reward_opt, 'Conv-P': avg_reward_conv, 'Desc-P': float(avg_reward_desc)})
            est_reward = curr_state[:, 0] * curr_state[:, 1]

            ###################################################################################################
            # optimal scheduling
            ###################################################################################################
            scheduling = np.argmax(est_reward)
            reward_opt = est_reward[scheduling]
            avg_reward_opt = (avg_reward_opt * t + reward_opt) / (t + 1)

            ###################################################################################################
            # conventional policy
            ###################################################################################################
            action_idx = get_action_conv_dqn(t, epsilon, systems[i].N, curr_state, ConvDQN_main[i])
            reward_conv = est_reward[action_idx]
            avg_reward_conv = (avg_reward_conv * t + reward_conv) / (t + 1)

            ConvDQN_buffer[i].push(torch.from_numpy(curr_state).float(),
                                   torch.tensor(action_idx).reshape((-1, 1)),
                                   torch.tensor(reward_conv.reshape((-1, 1))).float(),
                                   torch.from_numpy(next_state).float())

            if (t + 1) % train_interval == 0:
                if len(ConvDQN_buffer[i]) >= batch_size:
                    transitions = ConvDQN_buffer[i].sample(batch_size)
                    conv_dqn_train(transitions,
                                   batch_size,
                                   ConvDQN_main[i],
                                   ConvDQN_target[i],
                                   ConvDQN_optimizer[i],
                                   gamma)

            if (t + 1) % target_update_interval == 0:
                ConvDQN_target[i].load_state_dict(ConvDQN_main[i].state_dict())

            ###################################################################################################
            # descriptive policy
            ###################################################################################################
            action_idx, scheduling = get_action_desc_dqn(tt, epsilon, desc_curr_state, curr_corr_user,
                                                         systems[i].x_no, systems[i].p_no, DescDQN_main)
            reward_desc = est_reward[scheduling]
            avg_reward_desc = (avg_reward_desc * t + reward_desc) / (t + 1)

            if i != 2:
                DescDQN_buffer.push(torch.from_numpy(desc_curr_state).float(),
                                    torch.tensor(action_idx).reshape((-1, 1)),
                                    torch.tensor(reward_desc.reshape((-1, 1))).float(),
                                    torch.from_numpy(desc_next_state).float())

                if (t + 1) % train_interval == 0:
                    if len(DescDQN_buffer) >= batch_size:
                        transitions = DescDQN_buffer.sample(batch_size)
                        desc_dqn_train(transitions,
                                       batch_size,
                                       DescDQN_main,
                                       DescDQN_target,
                                       DescDQN_optimizer,
                                       gamma)

                if (t + 1) % target_update_interval == 0:
                    DescDQN_target.load_state_dict(DescDQN_main.state_dict())

            ###################################################################################################
            # dict
            ###################################################################################################
            dict_avg_reward_opt[tt] = avg_reward_opt
            dict_avg_reward_desc[tt] = avg_reward_desc
            dict_avg_reward_conv[tt] = avg_reward_conv

            # system update
            curr_state, desc_curr_state, curr_corr_user = next_state, desc_next_state, next_corr_user
            next_state = gen_state(systems[i].N, systems[i].x_max, systems[i].p_min, systems[i].p_max)
            desc_next_state, next_corr_user = to_descriptive(next_state, systems[i].N, systems[i].p_partition,
                                                             systems[i].x_max, systems[i].x_no, systems[i].p_no)

            tt = tt + 1
