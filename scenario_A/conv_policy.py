import numpy as np
import torch.nn.functional as F
from utils.utils import *


class ConvDQN(DQN):
    def __init__(self, name, fc_variable_no, user_no):
        super(ConvDQN, self).__init__(name, fc_variable_no, user_no*2, user_no)


def get_action_conv_dqn(t, epsilon, user_no, curr_state, main_network):
    rnd = np.random.rand(1)
    if t > 1e4:
        eepsilon = 1 / t
    else:
        eepsilon = epsilon
    # e-greedy 스케줄링
    if rnd < eepsilon:
        action_idx = np.random.randint(user_no)
    else:
        curr_state_tensor = torch.tensor(curr_state).float()
        Q_value = main_network(curr_state_tensor).detach()
        action_idx = np.argmax(Q_value)

    return action_idx


def conv_dqn_train(transitions, batch_size, main_network, target_network, optimizer, gamma):
    batch = Transition(*zip(*transitions))

    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.utility)
    next_state_batch = torch.cat(batch.next_state)
    state_batch_stack = torch.stack(batch.state)

    state_action_values = main_network(state_batch).gather(1, action_batch)
    next_state_values = target_network(next_state_batch).detach()
    next_state_values = next_state_values.max(1)[0]

    # 기대 Q 값 계산
    expected_state_action_values = (next_state_values.reshape((batch_size, 1)) * gamma) + reward_batch

    # Huber 손실 계산
    loss = F.mse_loss(state_action_values, expected_state_action_values)

    # 모델 최적화
    optimizer.zero_grad()
    loss.backward()
    for param in main_network.parameters():
        param.grad.data.clamp_(-100, 100)
    optimizer.step()