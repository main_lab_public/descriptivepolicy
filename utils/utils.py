import numpy as np
import random
import torch
import torch.nn as nn
from collections import namedtuple

Transition = namedtuple('Transition',
                        ('state', 'action', 'utility', 'next_state'))


def db2normal(a):
    return 10 ** (a / 10)


def normal2db(a):
    return 10 * np.log10(a)


class ReplayMemory(object):
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, *args):
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


class DQN(nn.Module):
    def __init__(self, name, fc_variable_no, x_dim, y_dim):
        super(DQN, self).__init__()
        self.name = name
        self.fc_variable_no = fc_variable_no

        self.x_dim = x_dim
        self.y_dim = y_dim

        self.fc_in = nn.Linear(self.x_dim, self.fc_variable_no)
        self.fc_hidden1 = nn.Linear(self.fc_variable_no, self.fc_variable_no)
        self.fc_hidden2 = nn.Linear(self.fc_variable_no, self.fc_variable_no)
        self.fc_out = nn.Linear(self.fc_variable_no, self.y_dim)
        self.relu = nn.ReLU()
        torch.nn.init.xavier_uniform_(self.fc_in.weight)
        torch.nn.init.xavier_uniform_(self.fc_hidden1.weight)
        torch.nn.init.xavier_uniform_(self.fc_hidden2.weight)
        torch.nn.init.xavier_uniform_(self.fc_out.weight)

    def forward(self, x):
        x = torch.reshape(x, [-1, self.x_dim])
        x = self.relu(self.fc_in(x))
        x = self.relu(self.fc_hidden1(x))
        x = self.relu(self.fc_hidden2(x))
        x = self.fc_out(x)
        return x
